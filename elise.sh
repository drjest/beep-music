#!/bin/bash
beep -f 659 -l 120  #  Treble E
beep -f 622 -l 120  #  Treble D#

beep -f 659 -l 120  #  Treble E
beep -f 622 -l 120  #  Treble D#
beep -f 659 -l 120  #  Treble E
beep -f 494 -l 120  #  Treble B
beep -f 587 -l 120  #  Treble D
beep -f 523 -l 120  #  Treble C

beep -f 440 -l 240  #  Treble A
beep -f 001 -l 120  #  pause
beep -f 262 -l 120  #  Middle C
beep -f 330 -l 120  #  Treble E
beep -f 440 -l 120  #  Treble A

beep -f 494 -l 240  #  Treble B
beep -f 001 -l 120  #  pause
beep -f 330 -l 120  #  Treble E
beep -f 415 -l 120  #  Treble G#
beep -f 494 -l 120  #  Treble B

beep -f 523 -l 240  #  Treble C
beep -f 001 -l 120  #  pause
beep -f 330 -l 120  #  Middle E
beep -f 659 -l 120  #  Treble E
beep -f 622 -l 120  #  Treble D#

beep -f 659 -l 120  #  Treble E
beep -f 622 -l 120  #  Treble D#
beep -f 659 -l 120  #  Treble E
beep -f 494 -l 120  #  Treble B
beep -f 587 -l 120  #  Treble D
beep -f 523 -l 120  #  Treble C

beep -f 440 -l 240  #  Treble A
beep -f 001 -l 120  #  pause
beep -f 262 -l 120  #  Middle C
beep -f 330 -l 120  #  Treble E
beep -f 440 -l 120  #  Treble A

beep -f 494 -l 240  #  Treble B
beep -f 001 -l 120  #  pause
beep -f 330 -l 120  #  Treble E
beep -f 523 -l 120  #  Treble C
beep -f 494 -l 120  #  Treble B

beep -f 440 -l 480  #  Treble A
beep -f 659 -l 120  #  Treble E
beep -f 622 -l 120  #  Treble D#

beep -f 659 -l 120  #  Treble E
beep -f 622 -l 120  #  Treble D#
beep -f 659 -l 120  #  Treble E
beep -f 494 -l 120  #  Treble B
beep -f 587 -l 120  #  Treble D
beep -f 523 -l 120  #  Treble C

beep -f 440 -l 240  #  Treble A
beep -f 001 -l 120  #  pause
beep -f 262 -l 120  #  Middle C
beep -f 330 -l 120  #  Treble E
beep -f 440 -l 120  #  Treble A

beep -f 494 -l 240  #  Treble B
beep -f 001 -l 120  #  pause
beep -f 330 -l 120  #  Treble E
beep -f 415 -l 120  #  Treble G#
beep -f 494 -l 120  #  Treble B

beep -f 523 -l 240  #  Treble C
beep -f 001 -l 120  #  pause
beep -f 330 -l 120  #  Middle E
beep -f 659 -l 120  #  Treble E
beep -f 622 -l 120  #  Treble D#

beep -f 659 -l 120  #  Treble E
beep -f 622 -l 120  #  Treble D#
beep -f 659 -l 120  #  Treble E
beep -f 494 -l 120  #  Treble B
beep -f 587 -l 120  #  Treble D
beep -f 523 -l 120  #  Treble C

beep -f 440 -l 240  #  Treble A
beep -f 001 -l 120  #  pause
beep -f 262 -l 120  #  Middle C
beep -f 330 -l 120  #  Treble E
beep -f 440 -l 120  #  Treble A

beep -f 494 -l 240  #  Treble B
beep -f 001 -l 120  #  pause
beep -f 330 -l 120  #  Treble E
beep -f 523 -l 120  #  Treble C
beep -f 494 -l 120  #  Treble B

beep -f 440 -l 240  #  Treble A
beep -f 001 -l 120  #  pause
beep -f 494 -l 120  #  Treble B
beep -f 523 -l 120  #  Treble C
beep -f 587 -l 120  #  Treble D

beep -f 659 -l 360  #  Treble E
beep -f 392 -l 120  #  Middle G
beep -f 698 -l 120  #  Treble F
beep -f 659 -l 120  #  Treble E

beep -f 587 -l 360  #  Treble D
beep -f 349 -l 120  #  Middle F
beep -f 659 -l 120  #  Treble E
beep -f 587 -l 120  #  Treble D

beep -f 523 -l 360  #  Treble C
beep -f 349 -l 120  #  Middle F
beep -f 587 -l 120  #  Treble D
beep -f 523 -l 120  #  Treble C
beep -f 494 -l 240  #  Middle B

beep -f 001 -l 120  #  pause
beep -f 349 -l 120  #  Middle F
beep -f 698 -l 120  #  Treble F
beep -f 001 -l 120  #  pause

beep -f 001 -l 120  #  pause
beep -f 698 -l 120  #  Treble F
beep -f 1396 -l 120 #  Treble F
beep -f 001 -l 120  #  pause
beep -f 001 -l 120  #  pause
beep -f 622 -l 120  #  Treble D#

beep -f 659 -l 120  #  Treble E
beep -f 001 -l 120  #  pause
beep -f 622 -l 120  #  Treble D#
beep -f 659 -l 120  #  Treble E
beep -f 622 -l 120  #  Treble D#

beep -f 659 -l 120  #  Treble E
beep -f 622 -l 120  #  Treble D#
beep -f 659 -l 120  #  Treble E
beep -f 494 -l 120  #  Treble B
beep -f 587 -l 120  #  Treble D
beep -f 523 -l 120  #  Treble C

beep -f 440 -l 240  #  Treble A
beep -f 001 -l 120  #  pause
beep -f 262 -l 120  #  Middle C
beep -f 330 -l 120  #  Treble E
beep -f 440 -l 120  #  Treble A

beep -f 494 -l 240  #  Treble B
beep -f 001 -l 120  #  pause
beep -f 330 -l 120  #  Treble E
beep -f 415 -l 120  #  Treble G#
beep -f 494 -l 120  #  Treble B

beep -f 523 -l 240  #  Treble C
beep -f 001 -l 120  #  pause
beep -f 330 -l 120  #  Middle E
beep -f 659 -l 120  #  Treble E
beep -f 622 -l 120  #  Treble D#

beep -f 659 -l 120  #  Treble E
beep -f 622 -l 120  #  Treble D#
beep -f 659 -l 120  #  Treble E
beep -f 494 -l 120  #  Treble B
beep -f 587 -l 120  #  Treble D
beep -f 523 -l 120  #  Treble C

beep -f 440 -l 960  #  Treble A